package org.jastadd;

public interface Consumer<T> {
  void accept(T v);
}
