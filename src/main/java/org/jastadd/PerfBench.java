/*
 * Copyright (c) 2017, The JastAdd Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *   3. The name of the author may not be used to endorse or promote
 *      products derived from this software without specific prior
 *      written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd;

import drast.model.SourceFile;
import drast.starter.ASTProvider;
import org.extendj.ast.ASTNode;
import org.extendj.ast.BodyDecl;
import org.extendj.ast.ConstructorDecl;
import org.extendj.ast.MethodAccess;
import org.extendj.ast.MethodDecl;
import org.extendj.ast.TypeDecl;
import org.extendj.ast.VarAccess;
import org.jastadd.benchmark.BenchmarkTask;
import org.jastadd.benchmark.FindErrors;
import org.jastadd.benchmark.NodeMapper;
import org.jastadd.benchmark.Result;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

public class PerfBench {
  private static boolean concurrent;

  private static ResourceBundle buildInfo = ResourceBundle.getBundle("org.jastadd.drast.BuildInfo");
  private static String[] csvColumns = {
    "index", "mode",
    "version", "concurrent", "errors", "warnings", "problemTime",
    "attributeCount", "attributeTime", "nodeTime", "numNodes",
    "heapSize", "threads"
  };

  /** Shuffle a collection using a PRNG. */
  public static <T, C extends Collection<T>> List<T> shuffle(C in, Random prng) {
    List<T> out = new ArrayList<>(in);
    Collections.shuffle(out, prng);
    return out;
  }

  public static void main(String[] args) {
    concurrent = buildInfo.getString("concurrent").equalsIgnoreCase("true");

    if (args.length < 2) {
      System.err.println("Usage: PerfBench <ARGFILE> <N> [THREADS]");
      System.err.println("    ARGFILE  = compiler arguments file");
      System.err.println("    N        = number of benchmark iterations");
      System.err.println("    THREADS  = parallel error checking thread count");
      System.exit(1);
      return;
    }

    String argfile = args[0];
    int N = Integer.parseInt(args[1]);

    // Build the list of benchmark configurations to measure.
    String threads = (args.length < 3) ? "4" : args[2];
    int threadCount = Integer.parseInt(threads);

    Collection<String> compileArgs = new ArrayList<>();
    compileArgs.add("@" + argfile);

    System.out.println(join(",", csvColumns));
    Random random = new Random(123);
    for (int i = 0; i < N; ++i) {
      benchmarkIteration(compileArgs, i, threadCount);
    }
  }

  private static String join(String sep, String[] strings) {
    StringBuilder buf = new StringBuilder();
    for (int i = 0; i < strings.length - 1; ++i) {
      buf.append(strings[i]);
      buf.append(sep);
    }
    if (strings.length > 0) {
      buf.append(strings[strings.length - 1]);
    }
    return buf.toString();
  }

  interface TaskBuilder {
    BenchmarkTask buildTask(Collection<SourceFile> files, int index);
  }

  /**
   * @param config the benchmark configuration index - defines which benchmark
   *    threads should run which tasks.
   */
  private static void benchmarkIteration(
      Collection<String> argList,
      final int index,
      int threadCount) {
    final TaskBuilder[] tasks = new TaskBuilder[threadCount];
    for (int task = 0; task < threadCount; ++task) {
      // Benchmark the CompilationUnit.problems() collection attribute.
      final int taskIndex = task;
      tasks[task] = new TaskBuilder() {
        @Override public BenchmarkTask buildTask(Collection<SourceFile> files, int i) {
          return new FindErrors(files, i + 1000 * taskIndex);
        }
      };
    }
    // Only one of the perf tasks are measured (not based on exec time).
    ASTProvider.parseAst(argList, new Consumer<Collection<SourceFile>>() {
      @Override public void accept(Collection<SourceFile> files) {
        benchmark(files, index, "perf", tasks);
      }
    });
  }

  /**
   * Add a key-value pair to a result multi-map.
   */
  public static void addResult(Map<String, List<Object>> map, String key, Object value) {
    List<Object> values = map.get(key);
    if (values == null) {
      values = new ArrayList<>();
      map.put(key, values);
    }
    values.add(value);
  }

  /**
   * Remove identical result values. Used to remove
   * duplicate warning and error counts from parallel
   * problems() runs in Benchmark 4.
   */
  public static void removeDups(Map<String, List<Object>> map, String key) {
    map.put(key, new ArrayList<>(new HashSet<>(map.get(key))));
  }

  private static void benchmark(Collection<SourceFile> files, int index,
      String mode, TaskBuilder... taskBuilders) {
    CountDownLatch startLatch = new CountDownLatch(taskBuilders.length);
    CountDownLatch taskLatch = new CountDownLatch(1);
    Collection<BenchmarkTask> tasks = new ArrayList<>();
    Collection<Thread> threads = new ArrayList<>();

    // An attribute evaluation lock to use when in sequential mode.
    ReentrantLock lock = new ReentrantLock(true); // Create a fair lock.

    for (TaskBuilder builder : taskBuilders) {
      BenchmarkTask task = builder.buildTask(files, index);
      tasks.add(task);
      if (concurrent) {
        // Concurrent mode.
        threads.add(task.concurrentTask(startLatch, taskLatch));
      } else {
        // Sequential mode.
        threads.add(task.sequentialTask(startLatch, taskLatch, lock));
      }
    }

    Map<String, List<Object>> results = new HashMap<>();

    addResult(results, "version", buildInfo.getString("version"));
    addResult(results, "index", index);
    addResult(results, "mode", mode);
    addResult(results, "threads", taskBuilders.length);
    addResult(results, "concurrent", concurrent ? "TRUE" : "FALSE");

    // Run GC now to avoid GC pass during benchmark.
    System.gc();

    // Start worker threads.
    for (Thread thread : threads) {
      thread.start();
    }

    try {
      // Wait for all workers to spin up.
      startLatch.await();
    } catch (InterruptedException e) {
      // Must re-throw as an unchecked exception so this method can be called in lambdas.
      throw new Error(e);
    }

    // Count down the latch to start concurrent tasks:
    taskLatch.countDown();

    // Wait for tasks to finish.
    for (Thread thread : threads) {
      try {
        thread.join();
      } catch (InterruptedException e) {
        throw new Error(e);
      }
    }

    // Collect all benchmark results.
    Runtime runtime = Runtime.getRuntime();
    addResult(results, "heapSize", runtime.totalMemory() - runtime.freeMemory());
    for (BenchmarkTask task : tasks) {
      for (Result result : task.results) {
        addResult(results, result.key, result.value);
      }
    }

    removeDups(results, "errors");
    if (results.get("errors").size() > 1) {
      // Print error in CSV output.
      System.out.println("ERROR: inconsistent error counts");
    }

    removeDups(results, "warnings");
    if (results.get("warnings").size() > 1) {
      // Print error in CSV output.
      System.out.println("WARNING: inconsistent warning counts");
    }

    // Print the result rows to the CSV output.
    int[] numVals = new int[csvColumns.length];
    for (int col = 0; col < csvColumns.length; ++col) {
      if (results.containsKey(csvColumns[col])) {
        numVals[col] = results.get(csvColumns[col]).size();
      } else {
        numVals[col] = 0;
      }
    }
    int[] colIndex = new int[csvColumns.length];
    String[] values = new String[csvColumns.length];
    while (true) {
      for (int col = 0; col < csvColumns.length; ++col) {
        if (results.containsKey(csvColumns[col])) {
          values[col] = "" + results.get(csvColumns[col]).get(colIndex[col]);
        } else {
          values[col] = "NA";
        }
      }
      System.out.println(join(",", values));
      boolean increment = true;
      for (int col = csvColumns.length - 1; col >= 0; --col) {
        if (increment) {
          colIndex[col] += 1;
          if (colIndex[col] >= numVals[col]) {
            colIndex[col] = 0;
          } else {
            increment = false;
          }
        }
      }
      if (increment) {
        // Done!
        break;
      }
    }
  }

  protected static List<VarAccess> findVars(Collection<SourceFile> files) {
    List<VarAccess> vars = new ArrayList<>();
    for (SourceFile file : files) {
      for (TypeDecl type : file.root.getTypeDeclList()) {
        for (BodyDecl member : type.getBodyDeclList()) {
          if (member instanceof MethodDecl) {
            MethodDecl method = (MethodDecl) member;
            addVars(vars, method.getBlock());
          } else if (member instanceof ConstructorDecl) {
            ConstructorDecl constructor = (ConstructorDecl) member;
            addVars(vars, constructor.getBlock());
          }
        }
      }
    }
    return vars;
  }

  private static void addVars(Collection<VarAccess> accesses, ASTNode<ASTNode> node) {
    if (node == null) {
      return;
    }
    for (ASTNode child : node.astChildren()) {
      if (child instanceof VarAccess) {
        VarAccess access = (VarAccess) child;
        accesses.add(access);
      } else {
        addVars(accesses, child);
      }
    }
  }

  protected static List<MethodAccess> findCalls(Collection<SourceFile> files) {
    List<MethodAccess> calls = new ArrayList<>();
    for (SourceFile file : files) {
      for (TypeDecl type : file.root.getTypeDeclList()) {
        for (BodyDecl member : type.getBodyDeclList()) {
          if (member instanceof MethodDecl) {
            MethodDecl method = (MethodDecl) member;
            addCalls(calls, method.getBlock());
          } else if (member instanceof ConstructorDecl) {
            ConstructorDecl constructor = (ConstructorDecl) member;
            addCalls(calls, constructor.getBlock());
          }
        }
      }
    }
    return calls;
  }

  private static void addCalls(Collection<MethodAccess> accesses, ASTNode<ASTNode> node) {
    if (node == null) {
      return;
    }
    for (ASTNode child : node.astChildren()) {
      if (child instanceof MethodAccess) {
        accesses.add((MethodAccess) child);
      } else {
        addCalls(accesses, child);
      }
    }
  }
}
