package drast.starter;

import drast.model.SourceFile;
import org.extendj.ast.ASTNode;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.Options;
import org.extendj.ast.Program;
import org.jastadd.Consumer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provides an AST by running the ExtendJ parser.
 */
public class ASTProvider {
  public static boolean parseAst(Collection<String> args,
      Consumer<Collection<SourceFile>> rootConsumer) {
    ASTNode.resetState();
    Program program = new Program();
    program.initBytecodeReader(Program.defaultBytecodeReader());
    program.initJavaParser(Program.defaultJavaParser());

    parseOptions(program, args);

    List<File> files = new ArrayList<>();
    for (String arg : program.options().files()) {
      File file = new File(arg);
      if (!file.isFile()) {
        System.err.println("Error: neither a valid option nor a filename: " + file);
      } else {
        files.add(new File(arg));
      }
    }

    Collection<SourceFile> sourceFiles = new ArrayList<>();
    try {
      // Parse source files:
      for (File file : files) {
        CompilationUnit cu = program.addSourceFile(file.getAbsolutePath());
        sourceFiles.add(new SourceFile(file, cu));
      }
      //Log.info("Parsing finished after %d ms", (System.currentTimeMillis() - start));
      rootConsumer.accept(sourceFiles);
      return true;
    } catch (Throwable e) {
      e.printStackTrace();
      //Log.error(e.getMessage());
      return false;
    }
  }

  private static void parseOptions(Program program, Collection<String> args) {
    initOptions(program);

    String[] argArray = new String[args.size()];
    args.toArray(argArray);
    program.options().addOptions(argArray);
  }

  private static void initOptions(Program program) {
    Options options = program.options();
    options.initOptions();
    options.addKeyOption("-version");
    options.addKeyOption("-print");
    options.addKeyOption("-g");
    options.addKeyOption("-g:none");
    options.addKeyOption("-g:lines,vars,source");
    options.addKeyOption("-nowarn");
    options.addKeyOption("-verbose");
    options.addKeyOption("-deprecation");
    options.addKeyValueOption("-classpath");
    options.addKeyValueOption("-cp");
    options.addKeyValueOption("-sourcepath");
    options.addKeyValueOption("-bootclasspath");
    options.addKeyValueOption("-extdirs");
    options.addKeyValueOption("-d");
    options.addKeyValueOption("-encoding");
    options.addKeyValueOption("-source");
    options.addKeyValueOption("-target");
    options.addKeyOption("-help");
    options.addKeyOption("-O");
    options.addKeyOption("-J-Xmx128M");
    options.addKeyOption("-recover");
    options.addKeyOption("-XprettyPrint");
    options.addKeyOption("-XdumpTree");

    // Non-javac options.
    options.addKeyOption("-profile"); // Output profiling information.
    options.addKeyOption("-debug"); // Extra debug checks and information.
  }
}

