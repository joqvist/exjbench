#!/bin/sh

set -eu

gradle clean jar -Pconcurrent=false && cp benchmark.jar benchmark-seq.jar

gradle clean jar -Pconcurrent=true && cp benchmark.jar benchmark-con.jar

scp benchmark-con.jar benchmark-seq.jar ibm:bench/
