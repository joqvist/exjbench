#/bin/bash

# Compile a single test program.

set -e

interrupted()
{
    exit $?
}

trap interrupted SIGINT

if [ $# -lt "1" ]; then
    echo "Usage: $0 SYSTEM_NAME"
	echo "    SYSTEM_NAME   The name of a Qualitas system to compile"
    exit 1
fi

SYSTEM="$1"
ARGFILE="${ARGFILE:-systems/$SYSTEM}"

if [ ! -e "$ARGFILE" ]; then
	ARGFILE="$SYSTEM"
fi

if [ ! -e "$ARGFILE" ]; then
	echo "Error: did not find argument file: ${ARGFILE}"
	exit 1
fi

# Path to the Qualitas Corpus:
EXJ_PATH="extendj/extendj.jar"

if [ ! -e "$EXJ_PATH" ]; then
  (cd 'extendj'; ./gradlew jar --offline)
fi

if [ ! -e "$EXJ_PATH" ]; then
	echo "Error: ExtendJ Jar file not found: ${EXJ_PATH}"
	exit 1
fi

if [ ! -d "logs" ]; then
	mkdir "logs"
fi


# echo 'Cleaning temporary directory "tmp"...'
if [ -d "tmp" ]; then
	rm -r "tmp"
fi
mkdir "tmp"

set +e  # Execution should now continue even if compilation fails.

# The ${@:2} expands to the command-line arguments after the system name,
# which are the optional arguments to send to javac.
echo "Compiling ${SYSTEM} with javac..."
TSTART=$(date +%s.%N)
javac ${@:2} "@${ARGFILE}" &> "logs/${SYSTEM}.javac.log"
RESULT=$?
TEND=$(date +%s.%N)
TDIFF=$(echo "$TEND - $TSTART" | bc -l)
echo "    result: ${RESULT} (${TDIFF}s)"

# echo 'Cleaning temporary directory "tmp"...'
if [ -d "tmp" ]; then
	rm -r "tmp"
fi
mkdir "tmp"

echo "Compiling ${SYSTEM} with ExtendJ..."
java -jar "$EXJ_PATH" -version > "logs/${SYSTEM}.extendj.log"
TSTART=$(date +%s.%N)
java -jar "$EXJ_PATH" "@${ARGFILE}" &>> "logs/${SYSTEM}.extendj.log"
RESULT=$?
TEND=$(date +%s.%N)
TDIFF=$(echo "$TEND - $TSTART" | bc -l)
echo "    result: ${RESULT} (${TDIFF}s)"

