# ExtendJ Benchmark Harness

This is a benchmark harness for benchmarking attributes in ExtendJ.

Clone the project using `git clone --recursive` to download the included ExtendJ submodule.

## Configuring Qualitas Corpus

1. Unpack Qualitas Corpus somewhere. To run the standard benchmarks you need only
   ant, antlr, aspectj, argouml, azureus, castor, cayenne, checkstyle, and cobertura.
2. Run `./setup-benchmarks.sh QC_PATH`, where `QC_PATH` is the path to the
   unpacked Qualitas Corpus.
3. Read the `ARTIFACT_README` file for instructions on running benchmarks after you
   have run the `setup_benchmarks` script.

The `setup_benchmarks` script configures compile paths for compiling the
Qualitas corpus systems with ExtendJ. The configured command-line flags are
stored in the directory `systems_10`.

You can also run `setup-qualitas.sh` to setup all Qualitas systems into the
`systems` directory.

## Running Benchmarks

The benchmark running process is pretty well documented in `ARTIFACT_README`.
However, before you run benchmarks the first time you need to build the Gradle project.
Run `./gradlew jar` to build the project.
