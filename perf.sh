#!/bin/bash

MEMLIMIT="-Xmx8g"
BENCHJAR_CON="benchmark-con.jar"
BENCHJAR_SEQ="benchmark-seq.jar"
SYSTEM=${1:-ant-1.8.4}
SYSTEM_PATH="systems_10/$SYSTEM"
ITERS=15

if [ ! -d out-perf ]; then
  mkdir out-perf
fi

echo "Sequential baseline"
java $MEMLIMIT -cp $BENCHJAR_SEQ org.jastadd.PerfBench $SYSTEM_PATH $ITERS 1 > out-perf/$SYSTEM-1-seq.csv

for THREADS in $(seq 1 8); do
  echo "Parallel $THREADS"
  java $MEMLIMIT -cp $BENCHJAR_CON org.jastadd.PerfBench $SYSTEM_PATH $ITERS $THREADS > out-perf/$SYSTEM-$THREADS-con.csv
done
